/**
 * Función llenar la vista con datos dummy
 */
function usarDatosDummy() {
  $.txtFieldUsername.value = 'penaldo';
  $.txtFieldPassword.value = 'penaldo';
}

/**
 * Función para inicializar el controlador
 */
function init() {
  $.loginButton.addEventListener('click', function(e) {
    var window = Alloy.createController('/inicio', {}).getView();
    window.open();
  });

  usarDatosDummy();

  $.window.open();
}

init();
