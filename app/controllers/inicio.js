var args = $.args;

/**
 * Función encargada de cerrar la ventana
 */
function close(){
    $.window.close();
}

/**
 * Función para inicializar el controlador
 */
function init() {
    $.btnMostrarMisCursos.addEventListener('click', function () {
        var window = Alloy.createController('/estudiantes/cursos/mis_cursos', {}).getView();
        window.open();
    });

    $.window.addEventListener('android:back', function(e) {
        e.cancelBubble = true;
    });
}

init();