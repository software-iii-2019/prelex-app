// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args,
    curso = args.itemData;

/**
  * Función encargada de cerrar la ventana
  */
function close() {
    $.window.close();
}

/**
 * Función encargada de llenar los campos de la vista con la información del curso
 */
function llenarCampos() {
    $.titulo.text = curso.nombre + ' - ' + curso.idioma;
    $.lblNombreProfesor.text = ' ' + curso.profesorNombre;
    $.lblJornada.text = ' ' + curso.jornada;
    $.lblFechaInicio.text = ' ' + curso.fechaInicio;
    $.lblFechaFin.text = ' ' + curso.fechaFin;
}

function init() {
    llenarCampos();
}


init();