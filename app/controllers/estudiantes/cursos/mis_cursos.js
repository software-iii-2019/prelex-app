// Arguments passed into this controller can be accessed via the `$.args` object directly or:
var args = $.args,
    xhr = new(require('ti.xhr'))();

var TAG = '[estudiantes/cursos/mis_cursos]'

args.id = '1000126110';

/**
 * Función encargada de cerrar la ventana
 */
function close() {
    $.window.close();
}

/**
 * Crea un curso como item para ser añadido a la lista
 * @param  {Object} itemData Información del curso contenida en un objeto
 * @return {Object} item Item que sera añadido a la lista
 */
function crearCursoItem(itemData) {
    var item = {
        itemData: itemData,
        nombreNivel: {
            text: itemData.nombre,
        },
        idioma: {
            text: itemData.idioma
        },
        properties: {}
    }
    return item;
}

/**
 * Hace una petición al backend para listar los cursos del estudiante
 */
function listarCursosEstudiante() {
    Alloy.Globals.loading.show('Cargando Cursos', false);
    var urlTemp = Alloy.Globals.environment.apiURL + '/matricula/grupos_estudiante/' + args.id
    xhr.GET({
        url: urlTemp,
        onSuccess: function(response) {
            Ti.API.info(TAG + ' - listarCursosEstudiante - success ' + JSON.stringify(response));
            if (!_.isEmpty(response.data)) {
                $.sectionGrupos.setItems(_.map(response.data, crearCursoItem));
            } else {
                alert('No se encontraron grupos')
            }
            Alloy.Globals.loading.hide();
        },
        onError: function(error) {
            Ti.API.error(TAG + ' - listarCursosEstudiante - error ' + JSON.stringify(error));
            alert('Ocurrio un error al cargar los cursos')
            Alloy.Globals.loading.hide();
        },
        extraParams: {
            // requestHeaders: [{
            //   key: 'Authorization',
            //   value: sessionManager.getAuthorizationString()
            // }],
            parseJSON: true
        }
    });
}

function cursosItemCLick(e) {
    var item = e.section.getItemAt(e.itemIndex),
        itemData = item.itemData;
    Ti.API.info(TAG + ' - cursosItemCLick - ' + JSON.stringify(itemData));
    var window = Alloy.createController('/estudiantes/cursos/curso_detalle', {iteamData: itemData}).getView();
    window.open();
}

/**
 * Función para inicializar el controlador
 */
function init() {
    setTimeout(function() {
        listarCursosEstudiante();
    }, 500);

    $.listCursos.addEventListener('itemclick', cursosItemCLick);
}

init();
