module.exports = {
    //"extends": "axway/env-alloy",
    "globals": {
        // declare globals here...
    },
    "rules": {
        // project specific overrides...
        "eol-last": ["warn", "never"],
        "indent": ["error", 4],
        "array-bracket-spacing": ["warn", "never"],
        "curly": ["warn", "multi-line"],
        "quotes": [2, "single", {
            "avoidEscape": true
        }],
        "no-undef": [0, {
            "ignoreBuiltinGlobals": ["$model"]
        }],
    },
    "plugins": [
      "alloy"
    ]
};
